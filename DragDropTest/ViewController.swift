//
//  ViewController.swift
//  DragDropTest
//
//  Created by Joe Lo on 2018/7/18.
//  Copyright © 2018年 Joe Lo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    var orignalPoint:CGPoint?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        addGestureRecognizers()
    }
    func addGestureRecognizers()
    {
        let gestureRecognizers = [UITapGestureRecognizer(target: self, action: #selector(action(recognizer:))),
                                  UIPanGestureRecognizer(target: self, action: #selector(action(recognizer:)))]
        let gestureRecognizers2 = [UITapGestureRecognizer(target: self, action: #selector(action(recognizer:))),
                                   UIPanGestureRecognizer(target: self, action: #selector(action(recognizer:)))]
        gestureRecognizers.forEach { (recognizer) in
            button1.addGestureRecognizer(recognizer)
        }
        gestureRecognizers2.forEach { (recognizer) in
            button2.addGestureRecognizer(recognizer)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func buttonTouchDown(_ sender: UIButton) {
        orignalPoint = nil
    }
    
    @objc func action(recognizer:UIGestureRecognizer)
    {
        switch recognizer {
        case let tap as UITapGestureRecognizer:
            let count = tap.numberOfTouches
            for i in 0 ..< count
            {
                print("Tap:\(tap.location(ofTouch: i, in: recognizer.view))")
            }
        case let pan as UIPanGestureRecognizer:
            let count = pan.numberOfTouches
            for i in 0 ..< count
            {
                let point = pan.location(ofTouch: i, in: self.view)
                print("Pan \(i):\(pan.location(ofTouch: i, in: recognizer.view))")
                if let lastPoint = orignalPoint,
                    var frame = recognizer.view?.frame
                {
                    print("orig \(frame)")
                    frame.origin.x += point.x - lastPoint.x
                    frame.origin.y += point.y - lastPoint.y
                    recognizer.view?.frame = frame
                    print("new \(frame)")
                }
                orignalPoint = point
            }
        default:
            break
        }
    }
}

